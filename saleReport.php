<?php
header('Content-type: text/xml');

$site = "www.smstadka.com/apis/saleReport.php?username=USERNAME&password=PASSWORD&from=FROM&to=TO";

include("../../config/bootstrap.php");
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die ('Error connecting to mysql');
mysql_select_db(DB_DB);

$username = trim(urldecode($_REQUEST['username']));
$password = trim(urldecode($_REQUEST['password']));
$from = trim(urldecode($_REQUEST['from']));
$to = trim(urldecode($_REQUEST['to']));

if(empty($username) || empty($password) || empty($from) || empty($to)){
	echo "<error>
		<code>0</code>
		<message>Missing/Invalid Parameter(s). Please check the request url.</message>
		</error>";
}
else {
	$from_dates = explode('-',$from);
	$to_dates = explode('-',$to);
	if(!checkdate($from_dates[1],$from_dates[2],$from_dates[0]) || !checkdate($to_dates[1],$to_dates[2],$to_dates[0])){
		echo "<error>
		<code>5</code>
		<message>Format of 'From Date' or 'To Date' is not valid.</message>
		</error>";
	}
	else if($from > $to){
		echo "<error>
		<code>6</code>
		<message>From date cannot be greater than To date</message>
		</error>";
	}
	else {
		$sql = "SELECT id FROM vendors WHERE username = '$username' and password = '$password'";
		$result = mysql_query($sql);
		if($row = mysql_fetch_array($result)){
			
			$query = "SELECT products.name,  vendor_trans_id, vendor_retail_code, vendors_activations.timestamp, products.price,vendors_activations.ref_code FROM vendors_activations,products_users,products WHERE vendor_id = " . $row['id'] . " AND Date(vendors_activations.timestamp) >= '$from' AND Date(vendors_activations.timestamp) <= '$to' AND products_users.id = productuser_id AND products.id = products_users.product_id ORDER by vendors_activations.timestamp";	
			$data = mysql_query($query);
			$xml = "<report>";
			$xml .= "<totalTransactions>".mysql_num_rows($data)."</totalTransactions>";
			$totalsale = 0;
			while($trans = mysql_fetch_array($data)){
				$xml1 .= "<transaction>
				<transId>".$trans[1]."</transId>
				<refId>".$trans[5]."</refId>
				<product>".$trans[0]."</product>
				<price>".$trans[4]."</price>
				<retailerCode>".$trans[2]."</retailerCode>
				<time>".$trans[3]."</time>";
				$xml1 .= "</transaction>";
				$totalsale += $trans[4];
			}
			$xml .= "<totalSale>".$totalsale."</totalSale>";
			$xml .= "<allTransactions>";
			$xml .= $xml1;
			$xml .= "</allTransactions>";
			$xml .= "</report>";
			echo $xml;
		}
		else {
			echo "<error>
				<code>1</code>
				<message>Wrong UserName or Password</message>
				</error>";
		}
	}
}

?>