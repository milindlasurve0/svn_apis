<?php

require("../../vendors/Predis/Autoloader.php");
include('../../vendors/logger/main/php/Logger.php');

/*$default_server_config = array(
        "scheme" => "tcp",
        "host" => "127.0.0.1",
        "port" => 6300,
        "password"=>'$avail$p@y!'
 );
*/
$default_server_config = array(
        "scheme" => "tcp",
        "host" => "redis1.oq14zy.0001.use1.cache.amazonaws.com",
        "port" => 6300,
 );

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
$headers .= "From: admin@smstadka.com\r\n";
$headers .= "Reply-To: admin@smstadka.com\r\n";

class vmnManager {
    //put your code herenewPHPClass
    var $redisObj;
    var $vmnQ;
    var $logger;
    //var $payTQ;
    var $Q;
    
    public function __construct() {
        try{
            global $headers;
            global $default_server_config;
            Predis\Autoloader::register();
            $this->redisObj = new Predis\Client($default_server_config);
        }  catch (Exception $e){
                echo "Couldn't connected to Redis";
                echo $e->getMessage();
                $this->notifyAlert($e->getMessage());
        }
        $this->Q['vmnQ'] = "VMN_Queue";
        $this->Q['payTQ'] = "PAY1_TRANS_Queue";
    }
    
    /**
     * 
     * @return type redis Object
     */
    public function get_rconnection(){
        return $this->redisObj;
    }
    
    public function generate_Queue_name($name) {
        return $qname = is_null($name)?$this->Q['vmnQ']:$this->Q[$name];
    }

    public function push_vmn_queue($parameter = null, $Qname = null) {
        try {
            if ($parameter != NULL) {
                $parameter = $this->format_json($parameter);
                $this->redisObj->lpush($this->Q['vmnQ'], $parameter);
            } else {
                echo "improper data : " . $parameter;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->notifyAlert($e->getMessage());
        }
    }

    public function push_transaction_queue($parameter = null) {
        try {
            if ($parameter != NULL) {
                $parameter = $this->format_json($parameter);
                $this->redisObj->lpush($this->Q['payTQ'], $parameter);
            } else {
                echo "improper data : " . $parameter;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $this->notifyAlert($e->getMessage());
        }
    }
    
    public function set_key_with_expiry($key, $val = "0", $seconds = NULL) {
        try {
            $this->redisObj->set($key, $val);
            if ($seconds != NULL) {
                $this->redisObj->expire($key, $seconds);
            }
        } catch (Exception $ex) {
            echo 'Exception occured in pushing set in redis with key : ' . $key . " and time : " . $seconds;
            echo str($ex);
        }
    }

    public function check_key_exist($key){
        try{
            return $this->redisObj->exists($key);            
        } catch (Exception $ex) {
            echo 'Exception occured while checking existance of key : '.$key;
            echo str($ex);
        }
    }

    public function pull_vmn_queue($Qname=NULL){
        //$Queue_name = $this->generate_Queue_name($Qname);
        $data = $this->redisObj->rpop($this->Q['vmnQ']);
        return $data;
    }
    
    public function pull_transaction_queue(){
        return $this->redisObj->rpop($this->Q['payTQ']);
    }
    
 	public function length_transaction_queue(){
        return $this->redisObj->llen($this->Q['payTQ']);
    }
    
    public function is_json($str){
        return $result = is_array($str)?false:json_decode($str,false) != null;
    }
    
    public function format_json($param) {
        return $this->is_json($param) ? $param : json_encode($param);
    }

    public function notifyAlert($errorString) {
        mail('tadka@mindsarray.com', 'VMN: Issue with Redis Connection. Error : ' . str($errorString), $headers);
    }

    public function logger($loggername = '',$loggerfilename='default'){
        try{
        $this->logger = Logger::getLogger($loggername);
        Logger::configure(array(
            'rootLogger' => array(
                'appenders' => array('default'),
            ),
            'appenders' => array(
                'default' => array(
                    'class' => 'LoggerAppenderFile',
                    'layout' => array(
                        'class' => 'LoggerLayoutPattern'
                    ),
                    'params' => array(
                        'file' => '/var/log/apps/'.$loggerfilename.'_'.date('Ymd').'.log',
                        'append' => true
                    )
                )
            )
        ));     
        } catch (Exception $ex) {
            print str($ex);
        }
        return $this->logger;
    }
    
}
