<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include("../../config/bootstrap.php");
include("../../config/database.php");
//require("../../vendors/predis/autoload.php");
require("vmnManager.php");
/**
 * headers variable is set to use while sending alert to user via mail
 */
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
$headers .= "From: admin@smstadka.com\r\n";
$headers .= "Reply-To: admin@smstadka.com\r\n";


$dbconfig = new DATABASE_CONFIG();
$db_var = $dbconfig->default;
$db_var['headers'] = $headers;
function dbconnect($db_var){
    $conn = mysql_connect($db_var['host'], $db_var['login'], $db_var['password']) or die(mail('nandan@mindsarray.com', 'smstadka async process : DB connection refused inside sms incoming script', 'Reason: ' . mysql_error(), $db_var['headers']));
    mysql_select_db($db_var['database'],$conn);
    return $conn;
}

$vmnObj1 = new vmnManager();
$logger = $vmnObj1->logger('SMS_LOG_QUERY', 'SMS_LOG_QUERY');
$redisObj = $vmnObj1->get_rconnection();
$queue_name = "SMS_LOG_QUERY";
$con = dbconnect($db_var);

while (true) {
    if(!$redisObj){
        $redisObj = $vmnObj1->get_rconnection();
    }
    
    if($redisObj->llen($queue_name) < 1) continue;
    $uid = uniqid();
    $query = $redisObj->rpop($queue_name);
    $logger->info("query ($uid) : ".$query);
    if(!$con){
        $con = dbconnect($db_var);
    }
    
    if(!mysql_query($query,$con)){
        mail('nandan@mindsarray.com', 'smstadka async process : DB connection refused inside sms incoming script', 'Reason: ' . mysql_error(), $headers);
        $logger->info("query ($uid). : failed : ".json_encode(mysql_error($con))." : ".json_encode($con));
    }else{
        $logger->info("query ($uid). : success : ");
    }
}
