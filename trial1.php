<?php
$module = 'win_serial';
 
if (extension_loaded($module)) {
} else {
 die("Module $module is not compiled into PHP");
}
 
/*$functions = get_extension_funcs($module);
echo "Functions available in the $module extension:<br>\n";
foreach($functions as $func) {
    echo $func."<br>";
}*/

echo "<p>";
$str = ser_version();
echo "Version: $str";

echo "<p>";
echo "Open port";
ser_open("\\\\.\\COM38", 9600, 8, "None", 1, "None");

echo "<p>";
if (ser_isopen() == true )
    echo "Port is open<br>\r\n";
else
    echo "Port is closed<br>\r\n";

echo "<p>";
//ser_setDTR(False);
//ser_setRTS(True);
ser_flush(true,true);
sleep(1);

$nr = ser_inputcount( );
echo "$nr bytes available";

echo "<p>";
echo "Setting text mode";
ser_write("AT+CMGF=1\r\n");
ser_flush(true,true);
echo "<p>";
echo "Waiting";
sleep(1);

echo "<p>Reading answer";

$str = ser_read();
echo $str;
ser_flush(true,true);
echo "<p>";
echo "List SMS";
ser_write("AT+CMGL=\"ALL\"\r\n");
ser_flush(true,true);
echo "<p>";
echo "Waiting";
sleep(5);

echo "<p><pre>";
$str = ser_read();
echo $str;

echo "<p>";
echo "Close port";
ser_close();
?>